Django landing page project.

Stack: python 2, Django 1.8, Bootstrap

How to run:

Clone the repository.

Install requirements.

pip install -r requirements.txt

Run the server from the root directory of the project.

python manage.py runserver
